# Comparison of different codes

Stellananzahl:		Anzahl stellen eines Codeworts
Bewertbarkeit:		Ist jeder Stelle eine bestimmte Wertigkeit zugewiesen? z.B.2² 2³
Gewicht:            Anzahl mit 1 belegten Stellen (0101 = Gewicht 2)
Minimaldistanz:	    Minimum der sich ändernden Stellen
Maximaldistant:	    Maximum der sich ändernden Stellen 
Hammingdistanz:     Anzahl minimum an "Änderungen"
Stetig:				Ist die Distanz zwischen allen Codewörtern konstant?
Redundanz:			Wortlänge - (log(Informationen) / log(2))

## 8-4-2-1 Code
Stellananzahl:		2
Bewertbarkeit:		Ja
Gewicht:            0-3
Minimaldistanz:	    1
Maximaldistant:	    4
Hammingdistanz:     1
Stetig:				Nein
Redundanz:			

## 1 aus 10 Code
Stellananzahl:		10
Bewertbarkeit:		Ja
Gewicht:            1
Minimaldistanz:	    2
Maximaldistant:	    2
Hammingdistanz:     2
Stetig:				Ja
Redundanz:			6.78...
// 10 - (log(10) / log(2))

## 7-Segment-Code
Stellananzahl:		7
Bewertbarkeit:		Nein
Gewicht:            2-7
Minimaldistanz:	    1
Maximaldistant:	    6
Hammingdistanz:     1
Stetig:				Nein
Redundanz:			3.678....
// Maximaldistanz - (log(5) / log(2))

## Gray-Code
Stellananzahl:		infinite
Bewertbarkeit:		Nein
Gewicht:            0-infinite
Minimaldistanz:	    1
Maximaldistant:	    1
Hammingdistanz:     1
Stetig:				Ja
Redundanz:			N/A