# Comparison Number systems

## Binary

Stellananzahl:		infinite
Bewertbarkeit:		Ja; 2^x
Minimaldistanz:	    1
Maximaldistant:	    infinite
Hammingdistanz:     1?
Stetig:				Ja
Redundanz:			Nein

## Dezimal

Stellananzahl:		infinite
Bewertbarkeit:		Ja; 10^x
Minimaldistanz:	    1
Maximaldistant:	    infinite
Hammingdistanz:     1?
Stetig:				Ja
Redundanz:			Nein

## Hexa-Dezimal

Stellananzahl:		infinite
Bewertbarkeit:		Ja; 16^x
Minimaldistanz:	    1
Maximaldistant:	    infinite
Hammingdistanz:     1?
Stetig:				Ja
Redundanz:			Nein
